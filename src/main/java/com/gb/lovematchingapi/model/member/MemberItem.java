package com.gb.lovematchingapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItem {
    private String name;
    private String isManName;
}

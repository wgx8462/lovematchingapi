package com.gb.lovematchingapi.model.member;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberPhoneChangeRequest {
    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;
}

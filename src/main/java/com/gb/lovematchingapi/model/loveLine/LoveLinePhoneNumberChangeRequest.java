package com.gb.lovematchingapi.model.loveLine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLinePhoneNumberChangeRequest {
    private String lovePhoneNumber;
}

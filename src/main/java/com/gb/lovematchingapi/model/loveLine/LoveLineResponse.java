package com.gb.lovematchingapi.model.loveLine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineResponse {
    private Long memberId;
    private String memberName;
    private String memberPhoneNumber;
    private String memberIsMan;
    private Long id;
    private String lovePhoneNumber;
}

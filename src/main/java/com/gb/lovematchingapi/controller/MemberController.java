package com.gb.lovematchingapi.controller;

import com.gb.lovematchingapi.model.member.MemberCreateRequest;
import com.gb.lovematchingapi.model.member.MemberItem;
import com.gb.lovematchingapi.model.member.MemberPhoneChangeRequest;
import com.gb.lovematchingapi.model.member.MemberResponse;
import com.gb.lovematchingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        return memberService.getMember(id);
    }

    @PutMapping("/phone/{id}")
    public String putMemberPhone(@PathVariable long id, @RequestBody MemberPhoneChangeRequest request) {
        memberService.putMemberPhone(id, request);
        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delMember(@PathVariable long id) {
        memberService.delMember(id);
        return "OK";
    }
}

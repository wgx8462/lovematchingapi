package com.gb.lovematchingapi.controller;

import com.gb.lovematchingapi.entity.Member;
import com.gb.lovematchingapi.model.loveLine.LoveLineCreateRequest;
import com.gb.lovematchingapi.model.loveLine.LoveLineItem;
import com.gb.lovematchingapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import com.gb.lovematchingapi.model.loveLine.LoveLineResponse;
import com.gb.lovematchingapi.service.LoveLineService;
import com.gb.lovematchingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-line")
public class LoveLineController {
    private final MemberService memberService;
    private final LoveLineService loveLineService;

    @PostMapping("/new/member-id/{memberId}")
    public String setLoveLine(@PathVariable long memberId, @RequestBody LoveLineCreateRequest request) {
        Member member = memberService.getData(memberId);
        loveLineService.setLoveLine(member, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<LoveLineItem> getLoveLines() {
        return loveLineService.getLoveLineS();
    }

    @GetMapping("/detail/{id}")
    public LoveLineResponse getLoveLine(@PathVariable long id) {
        return loveLineService.getLoveLine(id);
    }


    @PutMapping("/phone-number/love-line-id/{loveLineId}")
    public String putPhoneNumber(@PathVariable long loveLineId, @RequestBody LoveLinePhoneNumberChangeRequest request) {
        loveLineService.putPhoneNumber(loveLineId, request);

        return "OK";
    }

}

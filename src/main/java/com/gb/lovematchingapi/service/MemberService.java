package com.gb.lovematchingapi.service;

import com.gb.lovematchingapi.entity.Member;
import com.gb.lovematchingapi.model.member.MemberCreateRequest;
import com.gb.lovematchingapi.model.member.MemberItem;
import com.gb.lovematchingapi.model.member.MemberPhoneChangeRequest;
import com.gb.lovematchingapi.model.member.MemberResponse;
import com.gb.lovematchingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setIsMan(request.getIsMan());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList) {
            MemberItem addData = new MemberItem();
            addData.setName(member.getName());
            addData.setIsManName(member.getIsMan() ? "남자" : "여자");
            result.add(addData);
        }

        return result;

    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setIsManName(originData.getIsMan() ? "남자" : "여자");

        return response;
    }

    public void putMemberPhone(long id, MemberPhoneChangeRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setPhoneNumber(request.getPhoneNumber());
        memberRepository.save(originData);
    }

    public void delMember(long id) {
        memberRepository.deleteById(id);
    }
}

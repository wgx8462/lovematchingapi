package com.gb.lovematchingapi.service;

import com.gb.lovematchingapi.entity.LoveLine;
import com.gb.lovematchingapi.entity.Member;
import com.gb.lovematchingapi.model.loveLine.LoveLineCreateRequest;
import com.gb.lovematchingapi.model.loveLine.LoveLineItem;
import com.gb.lovematchingapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import com.gb.lovematchingapi.model.loveLine.LoveLineResponse;
import com.gb.lovematchingapi.repository.LoveLineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoveLineService {
    private final LoveLineRepository loveLineRepository;

    public void setLoveLine(Member member, LoveLineCreateRequest request) {
        LoveLine addData = new LoveLine();
        addData.setMember(member);
        addData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(addData);
    }

    public List<LoveLineItem> getLoveLineS() {
        List<LoveLine> originList = loveLineRepository.findAll();
        List<LoveLineItem> result = new LinkedList<>();

        for (LoveLine loveLine : originList) {
            LoveLineItem addItem = new LoveLineItem();
            addItem.setMemberId(loveLine.getMember().getId());
            addItem.setMemberName(loveLine.getMember().getName());
            addItem.setMemberPhoneNumber(loveLine.getMember().getPhoneNumber());
            addItem.setLovePhoneNumber(loveLine.getLovePhoneNumber());
            result.add(addItem);
        }
        return result;
    }

    public LoveLineResponse getLoveLine(long id) {
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();
        LoveLineResponse response = new LoveLineResponse();
        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setMemberPhoneNumber(originData.getMember().getPhoneNumber());
        response.setMemberIsMan(originData.getMember().getIsMan() ? "남자" : "여자");
        response.setId(originData.getId());
        response.setLovePhoneNumber(originData.getLovePhoneNumber());

        return response;
    }

    public  void putPhoneNumber(long id, LoveLinePhoneNumberChangeRequest request) {
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();
        originData.setLovePhoneNumber(request.getLovePhoneNumber());
        loveLineRepository.save(originData);
    }
}

package com.gb.lovematchingapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "고백공격 App",
                description = "고백공격을 통해 마음에 안드는 사람을 퇴사시키자",
                version = "vi"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfigure {

    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("고백공격 API v1")
                .pathsToMatch(paths)
                .build();
    }

}

package com.gb.lovematchingapi.repository;

import com.gb.lovematchingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}

package com.gb.lovematchingapi.repository;

import com.gb.lovematchingapi.entity.LoveLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoveLineRepository extends JpaRepository<LoveLine, Long> {
}
